//
//  ZTAppDelegate.h
//  ZTObjcKit
//
//  Created by LuckyStrike-zhou on 11/07/2021.
//  Copyright (c) 2021 LuckyStrike-zhou. All rights reserved.
//

@import UIKit;

@interface ZTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
