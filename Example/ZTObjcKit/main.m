//
//  main.m
//  ZTObjcKit
//
//  Created by LuckyStrike-zhou on 11/07/2021.
//  Copyright (c) 2021 LuckyStrike-zhou. All rights reserved.
//

@import UIKit;
#import "ZTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ZTAppDelegate class]));
    }
}
