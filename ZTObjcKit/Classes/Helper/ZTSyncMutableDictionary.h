//
//  ZTZTSyncMutableDictionary.h
//  ZTObjcKitDemo
//
//  Created by LuckyStrike on 2021/7/6.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZTSyncMutableDictionary : NSObject
/*
 多线程下的安全字典 来自阿里
 */
- (nullable id)objectForKey:(_Nonnull id)aKey;

- (nullable id)valueForKey:(_Nonnull id)aKey;

- (NSArray * _Nonnull)allKeys;

- (void)setObject:(nullable id)anObject forKey:(_Nonnull id <NSCopying>)aKey;

- (void)removeObjectForKey:(_Nonnull id)aKey;

- (void)removeAllObjects;

- (NSMutableDictionary *_Nonnull)getDictionary;

@end

NS_ASSUME_NONNULL_END

